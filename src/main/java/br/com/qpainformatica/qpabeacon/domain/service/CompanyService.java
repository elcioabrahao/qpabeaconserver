package br.com.qpainformatica.qpabeacon.domain.service;

import br.com.qpainformatica.qpabeacon.domain.model.Company;
import br.com.qpainformatica.qpabeacon.domain.persistence.CompanyDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by elcio on 30/01/16.
 */
@Component
public class CompanyService {
    @Autowired
    private CompanyDAO db;

    // Lista todos os companies do banco de dados
    public List<Company> getCompanys() {
        List<Company> companies = db.getCompanies();
        return companies;
    }

    // Busca um company pelo id
    public Company getCompany(Long id) {
        return db.getCompanyById(id);
    }

    // Deleta o company pelo id
    @Transactional(rollbackFor = Exception.class)
    public boolean delete(Long id) {
        return db.delete(id);
    }

    // Salva ou atualiza o company
    @Transactional(rollbackFor = Exception.class)
    public boolean save(Company company) {
        db.saveOrUpdate(company);
        return true;
    }

    // Busca o company pelo nome
    public List<Company> findByName(String name) {
        return db.findByName(name);
    }

    public List<Company> findByTipo(String tipo) {
        return db.findByTipo(tipo);
    }
}

