package br.com.qpainformatica.qpabeacon.domain.persistence;

import br.com.qpainformatica.qpabeacon.domain.model.Company;
import org.hibernate.Query;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by elcio on 30/01/16.
 */
@Component
@SuppressWarnings("unchecked")
public class CompanyDAO extends HibernateDAO<Company> {
    public CompanyDAO() {
        // Informa o tipo da entidade para o Hibernate
        super(Company.class);
    }

    // Consulta um company pelo id
    public Company getCompanyById(Long id) {
        // O Hibernate consulta automaticamente pelo id
        return super.get(id);
    }

    // Busca um company pelo nome
    public List<Company> findByName(String nome) {
        Query q = getSession().createQuery("from Company where lower(nome)  like lower(?)");
        q.setString(0, "%" + nome +"%");
        return q.list();
    }

    // Busca um company pelo tipo
    public List<Company> findByTipo(String tipo) {
        Query q = getSession().createQuery("from Company where tipo=?");
        q.setString(0, tipo);
        List<Company> companies = q.list();
        return companies;
    }

    // Consulta todos os companies
    public List<Company> getCompanies() {
        Query q = getSession().createQuery("from Company");
        List<Company> companies = q.list();
        return companies;
    }

    // Insere ou atualiza um company
    public void salvar(Company c) {
        super.save(c);
    }

    // Deleta o company pelo id
    public boolean delete(Long id) {
        Company c = get(id);
        delete(c);
        return true;
    }
}
