package br.com.qpainformatica.qpabeacon.rest;

import br.com.qpainformatica.qpabeacon.domain.model.Company;
import br.com.qpainformatica.qpabeacon.domain.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import br.com.qpainformatica.qpabeacon.domain.util.Response;



import java.util.List;


/**
 * Created by elcio on 30/01/16.
 */
@Path("/empresa")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Component
public class CompanyResource {

    @Context
    HttpServletRequest request;

    @Autowired
    private CompanyService companyService;
    


    @GET
    public List<Company> get() {
        List<Company> companies = companyService.getCompanys();
        return companies;
    }

    @GET
    @Path("{id}")
    public Company get(@PathParam("id") long id) {
        Company c = companyService.getCompany(id);
        return c;
    }

    @GET
    @Path("/tipo/{tipo}")
    public List<Company> getByTipo(@PathParam("tipo") String tipo) {
        List<Company> companies = companyService.findByTipo(tipo);
        return companies;
    }

    @GET
    @Path("/nome/{nome}")
    public List<Company> getByNome(@PathParam("nome") String nome) {
        List<Company> companies = companyService.findByName(nome);
        return companies;
    }

    @DELETE
    @Path("{id}")
    public Response delete(@PathParam("id") long id) {
        companyService.delete(id);
        return Response.Ok("Company deletado com sucesso");
    }

    @POST
    public Response post(Company c) {
        companyService.save(c);
        return Response.Ok("Company salvo com sucesso");
    }

    @PUT
    public Response put(Company c) {
        companyService.save(c);
        return Response.Ok("Company atualizado com sucesso");
    }
    
    
}
