package br.com.qpainformatica.qpabeacon.rest;


import org.glassfish.jersey.media.multipart.MultiPartFeature;

import javax.ws.rs.core.Application;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by elcio on 30/01/16.
 */
public class QPAApplication extends Application {

    @Override
    public Set<Object> getSingletons() {
        Set<Object> singletons = new HashSet<>();
        // Suporte ao File Upload.
        singletons.add(new MultiPartFeature());
        return singletons;
    }

    @Override
    public Map<String, Object> getProperties() {
        Map<String, Object> properties = new HashMap<>();
        // Configura o pacote para fazer scan das classes com anotações REST.
        properties
                .put("jersey.config.server.provider.packages", "br.com.qpainformatica.qpabeacon");
        return properties;
    }


}
